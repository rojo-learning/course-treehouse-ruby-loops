
### Iterators ###

# each #
# pases a copy of each element of a collection to a block so it can be processed
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].each do |item|
  puts "The current array item is: #{item}"
end

{ name: 'David', surname: 'Rojo', location: 'México' }.each do |key, value|
  puts "The hash key is #{key} and the value is #{value}."
end

# each_key # hashes only
# pases a copy of each key in a hash to a block so it can be processed
{ name: 'David', surname: 'Rojo', location: 'México' }.each_key do |key|
  puts "Key: #{key}"
end

# each_value # hashes only
# pases a copy of each value in a hash to a block so it can be processed
{ name: 'David', surname: 'Rojo', location: 'México' }.each_value do |value|
  puts "Value: #{value}"
end

# times # integers only
# iterates a fixed number of cycles, equal to the integer that calls the method
5.times { puts "Hello!"                }
5.times { |item| puts "Hello! #{item}" }

# for #
for item in 1..3 do
  puts "The current item is #{item}."
end

for item in ['Programming', 'is', 'fun']
  puts "The current item is #{item}."
end
