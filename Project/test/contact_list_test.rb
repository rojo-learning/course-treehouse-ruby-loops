
require 'minitest/autorun'
require_relative '../lib/contact_list'

describe self do

  describe '#ask' do
    it 'returns an striped input string when the requested kind is string' do
      input    = 'and input\n'
      expected = input.chomp

      self.stub :gets, input do
        ask('', 'string').must_equal expected
      end
    end

    it 'returns an striped input string when there is not requested kind' do
      input    = 'and input\n'
      expected = input.chomp

      self.stub :gets, input do
        ask('').must_equal expected
      end
    end

    it 'returns an integer when the requested kind is number' do
      input    = '100\n'
      expected = input.chomp.to_i

      self.stub :gets, input do
        ask('', 'number').must_equal expected
      end
    end
  end

  describe '#ask_name' do
    fake_name = 'John Down'

    it 'returns a string' do
      self.stub :ask, fake_name do
        ask_name.must_be_instance_of String
      end
    end
  end

  describe '#ask_phone_numbers' do
    fake_answer = 'q'

    it 'returns an array' do
      self.stub :ask, fake_answer do
        ask_phone_numbers.must_be_instance_of Array
      end
    end
  end

  describe '#build_contact' do
    it 'returns a hash with name and phone number keys' do
      fake_name    = 'John Down'
      fake_numbers = ['1234567890', '1112223334']
      expected     = { name: fake_name, phone_numbers: fake_numbers }

      self.stub :ask_name, fake_name do
        self.stub :ask_phone_numbers, fake_numbers do
          build_contact.must_equal expected
        end
      end
    end
  end

  describe '#print_contact_list' do

    describe 'when the contact list has registers' do
      it 'shows the list correctly' do
        contacts = [{ name: 'John', phone_numbers: ['123456789'] }]
        expected = "Contact List\n----\nName: John\nPhone: 123456789\n----\n"

        proc { print_contact_list(contacts) }.must_output expected
      end
    end

    describe 'when the contact list has no registers' do
      it 'shows the list correctly' do
        contacts = []
        expected = "Contact List\n----\n----\n"

        proc { print_contact_list(contacts) }.must_output expected
      end
    end
  end

end
