
module Kernel

  def ask(question, kind='string')
    print question + ' '
    answer = gets.chomp
    answer = answer.to_i if kind == 'number'
    answer
  end

  def ask_name
    ask('What is the person name?')
  end

  def ask_phone_numbers
    [].tap do |phone_numbers|
      loop do
        answer = ask('Enter a phone number: ("q" to quit)')
        answer == 'q' ? break : phone_numbers.push(answer)
      end
    end
  end

  def build_contact
    {
      name: ask_name,
      phone_numbers: ask_phone_numbers
    }
  end

  def print_contact_list(list)
    puts 'Contact List'
    print_separator
    list.each  do |contact|
      puts "Name: #{contact[:name]}"
      contact[:phone_numbers].each do |phone|
        puts "Phone: #{phone}"
      end
    end
    print_separator
  end

  def print_separator(symbol='-')
    puts symbol * 4
  end

end
