
require_relative './lib/contact_list'

contacts = []

loop do
  answer = ask('Would you like to add a contact? (y/n)')
  answer == 'n' ? break : contacts.push(build_contact)
end

print_contact_list contacts
