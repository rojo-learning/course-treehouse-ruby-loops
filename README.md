
# Treehouse - Ruby Loops
Exercises and examples from the 'Loops' course, imparted by Jason
Seifer at [Treehouse](http://teamtreehouse.com/).

> In Ruby Loops, you'll learn how to automatically repeat statements using Ruby. You'll learn about the loop construct, including while loops, until loops, for loops, and more. You'll also learn the basics of iteration and then move on to creating a simple contact list management program.

## Topics
- *Ruby Loops*: A loop is used to repeat statements until a condition is met. Ruby offers several different kinds of loops: the loop, while loop, and until loop

- *Ruby Iteration*: Iteration can be used to loop through values in Arrays, Hashes, and other Ruby types. In this stage, we'll learn how to iterate over arrays and hashes. We'll also learn how to do other types of iteration using the for and times constructs.

## Project
- *Build a Simple Contact List*: Using the concepts of loops and iteration, we'll build a simple contact list program using Ruby. Along the way, we'll be writing methods to make our work easier.


---
This repository contains portions of code from the TreehouseTeam courses and is included only for reference under _fair use_.
