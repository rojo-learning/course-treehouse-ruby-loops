
### Loops ###

# loop #
# repeats the given instructions until the break instruction is reached
answer = ""

loop do
  print 'Do you want to continue? (y/n) '
  answer = gets.chomp.downcase
  break if answer =~ /n/i
end

# while #
# repeats the instructions while a defined condition evaluates to true
answer = ""

while answer != 'n'
  print 'Do you want me to repeat this pointless loop again? (y/n) '
  answer = gets.chomp.downcase
end

# until #
# stops repeating the instructions when the defined condition evaluates to true
answer = ""

until answer == 'n'
  print 'How about this loop? Do you want it to continue? (y/n) '
  answer = gets.chomp.downcase
end
